package com.test.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	public static Logger LOGGER = LoggerFactory.getLogger(ApiExceptionHandler.class);
	
	@ExceptionHandler(value = {ApiException.class})
	protected ResponseEntity<Object> handleApiException(ApiException apiException, WebRequest request) {

		LOGGER.error(apiException.getMessage(), apiException);
		String message = String.format("{\"mensaje\": \"%s\"}", apiException.getMessage()); 
		
		return handleExceptionInternal(apiException, message, new HttpHeaders(), apiException.getStatus(), request);

	}

}
