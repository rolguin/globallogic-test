package com.test.exception;

import org.springframework.http.HttpStatus;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private HttpStatus status;
	
	public ApiException(HttpStatus status, String message) {
		
		super(message);
		this.status = status;
		
	}

	public HttpStatus getStatus() {
		return status;
	}

}
