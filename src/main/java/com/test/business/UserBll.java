package com.test.business;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.dao.UserDao;
import com.test.entity.Phone;
import com.test.entity.User;
import com.test.exception.ApiException;
import com.test.utils.CommonUtils;

@Service
public class UserBll {

	public static Logger LOGGER = LoggerFactory.getLogger(UserBll.class);
	
	@Autowired UserDao userDao;
	
	public User addUser(User newUser) {
		
		try {
			
			//get user if exist
			String source = newUser.getEmail();
			byte[] bytes = source.getBytes("UTF8");
			UUID uuid = UUID.nameUUIDFromBytes(bytes);
			
			LOGGER.info("searching existing user for uuid : {}", uuid);
			User currentUser = userDao.findOne(uuid);
			
			if(currentUser != null) {
				
				throw new ApiException(HttpStatus.BAD_REQUEST, "El correo ya registrado");
				
			} 
			
			//TODO: Validate all required values
			
			//Validate email
			EmailValidator emailValidator = EmailValidator.getInstance(); 
			
			if(!emailValidator.isValid(newUser.getEmail())) {
				
				throw new ApiException(HttpStatus.BAD_REQUEST, "El email no tiene un formato v�lido");
				
			}
			
			//Validate pasword
			if(!CommonUtils.isPasswordValid(newUser.getPassword())) {
				
				throw new ApiException(HttpStatus.BAD_REQUEST, "El password no tiene un formato v�lido");
				
			}

			//add a new user
			newUser.setUuid(uuid);
			Date now = new Date();
			newUser.setCreated(now);
			newUser.setModified(now);
			newUser.setLastLogin(now);
			newUser.setIsActive(true);
			
			//generating a random UUID token
			String token = UUID.randomUUID().toString();
			newUser.setToken(token);
			
			if(newUser.getPhones() != null) {

				for(Phone phone : newUser.getPhones()) {

					phone.setUserId(uuid);

				}
				
			}
			
			LOGGER.info("new user created with token : {}", token);
			
			return userDao.save(newUser);
			
		} catch(UnsupportedEncodingException e) {
			
			throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
			
		}
		
	}

	
	
}
