package com.test.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.business.UserBll;
import com.test.exception.ApiException;
import com.test.entity.User;

@RestController
public class UserController {
	  
	public static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired UserBll userBll;
	
	@RequestMapping("/api/addUser")
	public User addUser(@RequestBody User newUser) {
		
		try {
		
			LOGGER.info("Starting add user service");
			return userBll.addUser(newUser);
			
		} catch(ApiException e) {
			
			throw e ;
			
		} catch(Throwable e) {
			
			throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()) ;
			
		}
		
	}
	
}