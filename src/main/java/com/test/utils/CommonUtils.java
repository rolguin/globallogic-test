package com.test.utils;

public class CommonUtils {

	public static boolean isPasswordValid(String password) {

		String pattern = "(?=.*[0-9].*[0-9])(?=.*[A-Z])(?=.*[a-z]).+";
		return password.matches(pattern);

	}
	
}